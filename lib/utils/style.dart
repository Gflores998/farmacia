import 'package:flutter/material.dart';

class MyStyle {
  static const Color textBodyLigth = Color(0xFFcccdd2);
  static const Color textBodyDark = Color(0xFFF6F7F9);


  static final lightTheme = ThemeData.light().copyWith(
    backgroundColor: const Color(0xFFFFFFFF),
    scaffoldBackgroundColor: const Color(0xFFFFFFFF),
    appBarTheme: const AppBarTheme(
      backgroundColor: Color(0xFF113BDF),
    ),
    bottomNavigationBarTheme: const BottomNavigationBarThemeData(
      selectedItemColor: Color(0xFF113BDF)
    ),
    primaryColor: const Color(0xFF113BDF),
    colorScheme: ColorScheme.fromSwatch().copyWith(
      secondary: const Color(0xFF70B6EB),
    )
  );


  static final darkTheme = ThemeData.dark().copyWith(
    backgroundColor: const Color(0xFF060709),
    scaffoldBackgroundColor: const Color(0xFF060709),
    appBarTheme: const AppBarTheme(
      backgroundColor: Color(0xFF353A85)
    ),
    bottomNavigationBarTheme: const BottomNavigationBarThemeData(
      selectedItemColor: Color(0xFF353A85)
    ),
    primaryColor: const Color(0xFF113BDF),
    colorScheme: ColorScheme.fromSwatch().copyWith(
      secondary: const Color(0xFF70B6EB),
    )
  );
}