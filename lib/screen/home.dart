import 'package:farmacia/screen/favoritos.dart';
import 'package:farmacia/screen/home_content.dart';
import 'package:farmacia/screen/landing.dart';
import 'package:farmacia/screen/mapa.dart';
import 'package:farmacia/screen/secundarias/carrito.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class HomePage extends StatefulWidget {
  const HomePage({ Key? key }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  var paginas = [
    const HomeContentPage(),
    const FavoritosPage(),
    const MapaPage()
  ];

  int paginaActual = 0;

  void _showToast(BuildContext context) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      const SnackBar(
        content: Text('Cerrando Sesión')
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Punto Farmacia"),
        actions: [
          IconButton(
            icon: const Icon(Icons.shopping_cart),
            tooltip: 'Carrito',
            onPressed: () {
              Navigator.of(context).push(
                PageRouteBuilder(
                  pageBuilder: (BuildContext context, _, __) => const CarritoPage(),
                ),
              );
            },
          ),
          IconButton(
            icon: const Icon(Icons.logout),
            tooltip: 'Cerrar Sesión',
            onPressed: () async {
              await FirebaseAuth.instance.signOut();
              _showToast(context);
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (context) =>
                    const LandingPage(),
                ),
              );
            },
          )
        ],
      ),

      body: paginas[paginaActual],

      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: paginaActual,
        onTap: (int index) {
          setState(() {
            paginaActual = index;
          });
        },
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Inicio'
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.star),
            label: 'Favoritos'
          ),
          
          BottomNavigationBarItem(
            icon: Icon(Icons.location_pin),
            label: 'Ubicaciones'
          ),
          
        ],
      ),
    );
  }
}