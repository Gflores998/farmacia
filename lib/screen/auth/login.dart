import 'package:farmacia/services/auth_service.dart';
import 'package:farmacia/widgets/btn_comun.dart';
import 'package:farmacia/widgets/input_comun.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../home.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({ Key? key }) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();

  final _emailTextController = TextEditingController();
  final _passwordTextController = TextEditingController();

  bool _isProcessing = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 32.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                  height: 50.0,
              ),
              Text(
                "Iniciar Sesión",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 28.0,
                  fontWeight: FontWeight.w600,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              const SizedBox(
                height: 50.0,
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    InputComun(
                      textController: _emailTextController, 
                      keyboardType: TextInputType.emailAddress,
                      hintText: "Correo Electrónico",
                      prefixIcon: const Icon(Icons.email)
                    ),
                    const SizedBox( height: 15),
                    InputComun(
                      textController: _passwordTextController, 
                      hintText: "Contraseña", 
                      obscureText: true,
                      prefixIcon: const Icon(Icons.password)
                    ),
                    const SizedBox( height: 15),
                    if (_isProcessing) const CircularProgressIndicator()
                    else BtnComun(
                      text: "Iniciar Sesión", 
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          setState(() { _isProcessing = true; });
                        }
                        User? user = await AuthService.signInUsingEmailPassword(
                          email: _emailTextController.text,
                          password:
                              _passwordTextController.text,
                        );
                        setState(() { _isProcessing = false; });

                        if (user != null) {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              builder: (context) =>
                                const HomePage(),
                            ),
                          );
                        }
                      }, 
                      esPrimario: true
                    )
                  ],
                )
              ),
            ]
          )
        ),
      ),
    );
  }
}