import 'package:farmacia/services/auth_service.dart';
import 'package:farmacia/providers/theme_provider.dart';
import 'package:farmacia/utils/style.dart';
import 'package:farmacia/widgets/btn_comun.dart';
import 'package:farmacia/widgets/input_comun.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';

import '../home.dart';
import 'login.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({ Key? key }) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  final _registerFormKey = GlobalKey<FormState>();
  final _nameTextController = TextEditingController();
  final _emailTextController = TextEditingController();
  final _passwordTextController = TextEditingController();

  bool _isProcessing = false;

  @override
  Widget build(BuildContext context) {

    final themeProvider = Provider.of<ThemeProvider>(context);

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 32.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                  height: 50.0,
              ),
              Text(
                "Registrarse",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 28.0,
                  fontWeight: FontWeight.w600,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              const SizedBox(
                height: 50.0,
              ),
              Form(
                key: _registerFormKey,
                child: Column(
                  children: [
                    InputComun(
                      textController: _nameTextController, 
                      keyboardType: TextInputType.text,
                      hintText: "Nombre",
                      prefixIcon: const Icon(Icons.person)
                    ),
                    const SizedBox( height: 15),
                    InputComun(
                      textController: _emailTextController, 
                      keyboardType: TextInputType.emailAddress,
                      hintText: "Correo Electrónico",
                      prefixIcon: const Icon(Icons.email)
                    ),
                    const SizedBox( height: 15),
                    InputComun(
                      textController: _passwordTextController, 
                      hintText: "Contraseña", 
                      obscureText: true,
                      prefixIcon: const Icon(Icons.password)
                    ),
                    const SizedBox( height: 15),
                    if (_isProcessing) const CircularProgressIndicator()
                    else BtnComun(
                      text: "Registrarse", 
                      onPressed: () async {
                        if (_registerFormKey.currentState!.validate()) {
                          setState(() { _isProcessing = true; });
                        }
                        User? user = await AuthService.registerUsingEmailPassword(
                          name: _nameTextController.text,
                          email: _emailTextController.text,
                          password: _passwordTextController.text
                        );
                        setState(() { _isProcessing = false; });

                        if (user != null) {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              builder: (context) =>
                                const HomePage(),
                            ),
                          );
                        }
                      }, 
                      esPrimario: true
                    )
                  ],
                )
              ),
              const SizedBox(height: 15),
              Text(
                "Al registrarse, acepta nuestros servicios y nuestra Política de privacidad.",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 14.0,
                  color: themeProvider.isDarkMode ? MyStyle.textBodyDark : MyStyle.textBodyLigth,
                ),
              ),
              const SizedBox(
                height: 30.0,
              ),
              Center(
                child: Wrap(
                  runAlignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Text(
                      "Ya tiene una cuenta?",
                      style: TextStyle(
                        fontSize: 16.0,
                        color: themeProvider.isDarkMode ? MyStyle.textBodyDark : MyStyle.textBodyLigth,
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                            builder: (context) =>
                              const LoginPage(),
                          ),
                        );
                      },
                      child: Text(
                        "Iniciar Sesión",
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ]
          )
        ),
      ),
    );
  }
}