import 'package:farmacia/models/favorito_model.dart';
import 'package:farmacia/screen/secundarias/producto_detalle.dart';
import 'package:farmacia/services/crud_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class FavoritosPage extends StatefulWidget {
  const FavoritosPage({ Key? key }) : super(key: key);

  @override
  State<FavoritosPage> createState() => _FavoritosPageState();
}

class _FavoritosPageState extends State<FavoritosPage> {

  final FirebaseAuth auth = FirebaseAuth.instance;
  late User _currentUser;

  @override
  void initState() {
    _currentUser = auth.currentUser!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(
              height: 20.0,
            ),
            RichText(
              text: const TextSpan(
                children: [
                  TextSpan(
                    text: 'Favoritos',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: Color.fromRGBO(152, 156, 173, 1)
                    )
                  )
                ]
              ),
            ),
            const SizedBox(
              height: 30.0,
            ),
            StreamBuilder<List<Favorito>>(
              stream: CrudService().obtenerFavoritos(_currentUser.uid),
              builder: (context, snapshot) {
                if(!snapshot.hasData){
                  return const Center(
                    child: Text("No Data")
                  );
                } else if(snapshot.hasError){
                  return const Center(
                    child: Text("An Error Occured")
                  );
                } else if(snapshot.hasData) {
                  return ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      Favorito fav = snapshot.data![index];
                      return Card(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget> [
                            ListTile(
                              leading: const Icon(Icons.star, color: Colors.yellow,),
                              title: Text(fav.nombre)
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                TextButton(
                                  child: const Text('Ver Producto'),
                                  onPressed: () {
                                    Navigator.push(context, MaterialPageRoute(
                                      builder: (_) =>
                                      ProductoDetalle(productoId: fav.productoId)
                                    ));
                                  },
                                ),
                                const SizedBox(width: 8),
                                TextButton(
                                  child: const Text('Eliminar'),
                                  onPressed: () async {
                                    await CrudService().eliminarFavorito(fav.id);
                                  },
                                ),
                                const SizedBox(width: 8),
                              ]
                            )
                          ]
                        ),
                      );
                    }, 
                    separatorBuilder: (BuildContext context, int index) {
                      return const SizedBox( height: 10.0);
                    }, 
                    itemCount: snapshot.data!.length
                  );
                } else {
                  return const Center(child: Text('Algo Salió mál'));
                }
              },
            )
            
          ]
        )
      ),
    );
  }
}