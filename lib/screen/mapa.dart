import 'dart:async';

import 'package:farmacia/services/crud_service.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:share_plus/share_plus.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MapaPage extends StatefulWidget {
  const MapaPage({ Key? key }) : super(key: key);

  @override
  State<MapaPage> createState() => _MapaPageState();
}

class _MapaPageState extends State<MapaPage> {

  final Completer<GoogleMapController> _controller = Completer();
  List<Marker> markers = [];

  static const CameraPosition _ss = CameraPosition(
    target: LatLng(13.696180388201162, -89.19578767722057),
    zoom: 14.4746
  );

  @override
  void initState(){
    super.initState();
    llenarMarkers();
  }

  llenarMarkers() async {
    var establecimiento = await CrudService().obtenerSucursales();
    for (var element in establecimiento) {
      markers.add(Marker(
        markerId: MarkerId(element.nombre),
        position: LatLng(element.lat, element.lng),
        icon: BitmapDescriptor.defaultMarkerWithHue(
          BitmapDescriptor.hueBlue
        ),
        onTap: () => {
          showDialog(
            context: context, 
            builder: (BuildContext context) {
              return AlertDialog(
                title: Container(
                  decoration: BoxDecoration(
                    color: const Color(0XFF2e4971),
                    border: Border.all(),
                    borderRadius: const BorderRadius.all(Radius.circular(6.0))
                  ),
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    element.nombre,
                    style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 17),
                  )
                ),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              child: const Text(
                                'Ubicación',
                                style: TextStyle(
                                    color: Colors.blue, fontSize: 12.0),
                              ),
                            ),
                            const Padding(
                              padding: EdgeInsets.only(left: 8.0),
                              child: Icon(
                                Icons.credit_card,
                                color: Colors.blue,
                                size: 16.0,
                                semanticLabel: 'Farmacias',
                              ),
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          text: 'Dirección: ',
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14.0), // default text style
                          children: <TextSpan>[
                            TextSpan(
                              text: element.direccion,
                              style: const TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: 14.0)),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          text: 'Horario: ',
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14.0), // default text style
                          children: <TextSpan>[
                            TextSpan(
                              text: element.horario,
                              style: const TextStyle(
                                fontWeight: FontWeight.w300,
                                fontSize: 14)
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      FloatingActionButton(
                        onPressed: () {
                          shareMarkerInfo(element.lat, element.lng);
                        },
                        child: const Icon(Icons.share),
                        backgroundColor: Colors.blue,
                        mini: true,
                      ),
                      FloatingActionButton(
                        onPressed: () {
                          navigateTo(element.lat, element.lng);
                        },
                        child: const Icon(Icons.navigation),
                        backgroundColor: Colors.green,
                        mini: true,
                      ),
                      TextButton(
                        child: const Text('Cerrar'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                ]
              );  
            }
          )
        }
      ));
    }
    setState(() {});
  }

  void shareMarkerInfo(double lat, double lng) {
    Share.share(
        'Comparto la ubicación de Mi Punto Farmacia: https://www.google.com/maps/dir/?api=1&destination=' +
            lat.toString() +
            ',' +
            lng.toString(),
        subject: 'Comparto la ubicación de Mi Punto Farmacia Preferida');
  }

  Future<void> navigateTo(double lat, double lng) async {
    final coords = Coords(double.parse(lat.toString()), double.parse(lng.toString()));
    const title = "Cajero Automático";
    final availableMaps = await MapLauncher.installedMaps;
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  for (var map in availableMaps)
                    ListTile(
                      onTap: () => map.showMarker(
                        coords: coords,
                        title: title,
                      ),
                      title: Text(map.mapName),
                      leading: SvgPicture.asset(
                        map.icon,
                        height: 30.0,
                        width: 30.0,
                      )
                    ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }




  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      initialCameraPosition: _ss,
      onMapCreated: (GoogleMapController controller) {
        _controller.complete(controller);
      },
      myLocationButtonEnabled: true,
      myLocationEnabled: true,
      markers: markers.map((mark) => mark).toSet(),
    );
  }
}

