import 'package:farmacia/models/categoria_model.dart';
import 'package:farmacia/screen/secundarias/productos.dart';
import 'package:farmacia/screen/secundarias/resultados.dart';
import 'package:farmacia/services/crud_service.dart';
import 'package:farmacia/widgets/barra_busqueda.dart';
import 'package:farmacia/widgets/saludo_usuario.dart';
import 'package:provider/provider.dart';
import 'package:farmacia/providers/theme_provider.dart';
import 'package:flutter/material.dart';

class HomeContentPage extends StatefulWidget {
  const HomeContentPage({ Key? key }) : super(key: key);

  @override
  State<HomeContentPage> createState() => _HomeContentPageState();
}

class _HomeContentPageState extends State<HomeContentPage> {
  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context);
  
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(height: 20.0),
            const SaludoUsuario(),
            const SizedBox(height: 20.0),
            BarraBusqueda(
              hintText: 'Buscar Medicamento', 
              obscureText: false, 
              suffixIcon: const Icon(Icons.search), 
              prefixIcon: const Icon(Icons.search), 
              borderRadius: 20.0,
              onSubmit: (val) {
                Navigator.push(context, MaterialPageRoute(
                  builder: (_) =>
                  ResultadosPage(filtro: val)
                ));
              },
            ),
            const SizedBox(height: 20.0),
            StreamBuilder<List<Categoria>>(
              stream: CrudService().obtenerCategorias(),
              builder: (context, snaptshot) {
                if (!snaptshot.hasData) {
                  return const Center(
                    child: Text('No hay información'),
                  );
                } else if(snaptshot.hasError) {
                  return const Center(
                    child: Text("Algo salio mál"),
                  );
                } else if (snaptshot.hasData) {
                    return GridView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 8.0,
                        crossAxisSpacing: 8.0
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        Categoria model = snaptshot.data![index];
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                              builder: (_) =>
                              ProductosPage(categoriaId: model.id)
                            ));
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18.0),
                              color:  themeProvider.isDarkMode ? const Color(0xFF142a43) : const Color(0xFFF7F6FE)
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.network(model.foto,
                                  height: 50
                                ),
                                const SizedBox(height: 10),
                                Text(model.nombre)
                              ],
                            ),
                          ),
                        );
                      },
                      itemCount: snaptshot.data!.length,
                    );
                } else {
                  return const Center(
                    child: Text("Algo salió mál"),
                  );
                }
              },
            )
          ],
        ),
      ),
    );
  }
}