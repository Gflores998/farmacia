import 'package:farmacia/models/carrito_model.dart';
import 'package:farmacia/models/favorito_model.dart';
import 'package:farmacia/models/producto_model.dart';
import 'package:farmacia/services/crud_service.dart';
import 'package:farmacia/widgets/app_bar_comun.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ProductoDetalle extends StatefulWidget {
  final String productoId;
  const ProductoDetalle({ Key? key, required this.productoId }) : super(key: key);

  @override
  State<ProductoDetalle> createState() => _ProductoDetalleState();
}

class _ProductoDetalleState extends State<ProductoDetalle> {

  late String productoSeleccionado;
  int cartItemCount = 1;

  bool cargando = true;
  bool esFavorito = false;
  late Producto producto;
  late Favorito favorito;

  final FirebaseAuth auth = FirebaseAuth.instance;
  late User _currentUser;

  @override
  void initState() {
    productoSeleccionado = widget.productoId;
    _currentUser = auth.currentUser!;
    cargarProducto();
    super.initState();
  }

  cargarProducto() async {
    producto = await CrudService().productoDetalle(productoSeleccionado);
    favorito = await CrudService().existeFav(producto.id, _currentUser.uid);
    setState(() {
      if (favorito.id != '') {
        esFavorito = true;
      }
      cargando = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppBarComun(),
      body: SingleChildScrollView(
        child: cargando ? 
        Container(
          child: const Center(
            child: CircularProgressIndicator(),
          ),
        ) : 
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    child: Row(
                      children: [
                        Expanded(
                          child: Container()
                        ),
                        Expanded(
                          child: Container(
                            color: Theme.of(context).primaryColor,
                          )
                        )
                      ],
                    ),
                  ),
                  SafeArea(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 50,
                        ),
                        productoImagenDetalle(context, producto),
                        Container(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              RichText(
                                textAlign: TextAlign.left,
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: producto.nombre,
                                      style: const TextStyle(
                                        height: 2.5,
                                        fontSize: 28.0,
                                        fontWeight: FontWeight.w600,
                                        color: Color.fromRGBO(34, 34, 34, 1)
                                      )
                                    ),
                                    TextSpan(
                                      text: ' ' +producto.breve,
                                      style: const TextStyle(
                                        fontSize: 28.0,
                                        color: Color.fromRGBO(34, 34, 34, 1)
                                      )
                                    )
                                  ]
                                ),
                              ),
                              const SizedBox(
                                height: 5.0,
                              ),
                              const Text(
                                'categoria',
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.grey
                                ),
                              ),
                              Container(
                                height: 90.0,
                                child: Row(
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.only(right: 15),
                                      height: 60,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8.0),
                                        border: Border.all(
                                          color: const Color.fromRGBO(230, 230, 230, 1)
                                        )
                                      ),
                                      child: Row(
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                cartItemCount += 1;
                                              });
                                            },
                                            child: Container(
                                              alignment: Alignment.center,
                                              height: 90.0,
                                              padding: const EdgeInsets.symmetric(horizontal: 16),
                                              child: const Text('+', style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 24,
                                                color: Color.fromRGBO(34, 34, 34, 1)
                                              )),
                                            ),
                                          ),
                                          Container(
                                            padding: const EdgeInsets.symmetric(
                                              horizontal: 16.0,
                                            ),
                                            child: Text(
                                              "$cartItemCount",
                                              style: const TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 24.0,
                                                color: Color.fromRGBO(
                                                    34, 34, 34, 1),
                                              ),
                                            )
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                cartItemCount = cartItemCount > 2 ? cartItemCount - 1 : 1;
                                              });
                                            },
                                            child: Container(
                                              alignment: Alignment.center,
                                              height: 90,
                                              padding: const EdgeInsets.symmetric(horizontal: 16),
                                              child: const Text(
                                                '-',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 24,
                                                  color: Color.fromRGBO(34, 34, 34, 1)
                                                ),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Text(
                                      producto.oferta ? 
                                      '\$${(producto.precioOferta * cartItemCount).toStringAsFixed(2)}' : 
                                      '\$${(producto.precio * cartItemCount ).toStringAsFixed(2)}',
                                      style: const TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w600,
                                        color: Color.fromRGBO(34, 34, 34, 1)
                                      ),
                                    )
                                  ],
                                )
                              ),
                              Text(
                                producto.descripcion,
                                style: const TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey,
                                  height: 1.40
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              accionesFooter(producto)
                            ],
                          ),
                        )
                      ],
                    )
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget productoImagenDetalle(BuildContext context, Producto producto) {
    return Hero(
      tag: producto.nombre, 
      child: Center(
        child: Container(
          padding: const EdgeInsets.all(24),
          height: MediaQuery.of(context).size.height * 0.38,
          width: MediaQuery.of(context).size.width * 0.85,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16),
            border: Border.all(
              color: Colors.grey
            ),
          ),
          child: Image.network(
            producto.foto
          ),
        ),
      )
    );
  }

  Widget accionesFooter(Producto producto) {
    return Row(
      children: [
        InkWell(
          onTap: () async {
            if (esFavorito) {
              await CrudService().eliminarFavorito(favorito.id);
              _showToast(context, 'Producto Eliminado como favorito');
              setState(() {
                esFavorito = false;
              });
            } else {
              Favorito fav = Favorito(
                id: 'a', 
                usuarioId: _currentUser.uid,
                productoId: producto.id,
                nombre: producto.nombre
              );
              await CrudService().agregarFavorito(fav);
              favorito = await CrudService().existeFav(producto.id, _currentUser.uid);
              setState(() {
                esFavorito = true;
              });
            }
              
          },
          child: Container(
            margin: const EdgeInsets.only(right: 15),
            width: 60,
            height: 60,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(
                color: const Color.fromRGBO(230, 230, 230, 1)
              )
            ),
            child: Icon(
              Icons.favorite,
              color: esFavorito ? Colors.red : Colors.black12
            ),
          ),
        ),
        Expanded(
          child: InkWell(
            onTap: () {
              agregarAlCarrito();
            },
            child: Container(
              margin: const EdgeInsets.only(right: 15),
              height: 60,
              padding: const EdgeInsets.symmetric( horizontal: 32),
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(
                  color: const Color.fromRGBO(230, 230, 230, 1)
                )
              ),
              child: const Center(
                child: Text(
                  'Agregar al Carrito',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 14
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  void agregarAlCarrito() async {
    Carrito cart = Carrito(
      id: 'a', 
      usuarioId: _currentUser.uid, 
      productoId: producto.id, 
      nombre: producto.nombre, 
      cantidad: cartItemCount,
      precio: producto.oferta ? producto.precioOferta : producto.precio
      );
    await CrudService().agregarAlCarrito(cart);
    _showToast(context, 'Producto Agregado al Carrito');
  }

  void _showToast(BuildContext context, msg) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text(msg)
      ),
    );
  }

}

