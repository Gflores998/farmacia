import 'package:farmacia/models/producto_model.dart';
import 'package:farmacia/services/crud_service.dart';
import 'package:farmacia/widgets/app_bar_comun.dart';
import 'package:farmacia/widgets/fila_producto.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ProductosPage extends StatefulWidget {
  final String categoriaId;

  const ProductosPage({ Key? key, required this.categoriaId }) : super(key: key);

  @override
  State<ProductosPage> createState() => _ProductosPageState();
}

class _ProductosPageState extends State<ProductosPage> {

  late String categoriaSeleccionada;

  @override
  void initState() {
    categoriaSeleccionada = widget.categoriaId;
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppBarComun(),
      body: SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(
              height: 20.0,
            ),
            // categoriaCabecera(),
            const SizedBox(
              height: 30.0,
            ),
            StreamBuilder<List<Producto>>(
              stream: CrudService().obtenerProductosPorCategoria(categoriaSeleccionada),
              builder: (context, snapshot) {
                if(!snapshot.hasData){
                  return const Center(
                    child: Text("No Data")
                  );
                } else if(snapshot.hasError){
                  return const Center(
                    child: Text("An Error Occured")
                  );
                } else if(snapshot.hasData) {
                  return ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      Producto producto = snapshot.data![index];
                      return FilaProducto(producto: producto);
                    }, 
                    separatorBuilder: (BuildContext context, int index) {
                      return const SizedBox( height: 10.0);
                    }, 
                    itemCount: snapshot.data!.length
                  );
                } else {
                  return const Center(child: Text('Algo Salió mál'));
                }
              },
            )
          ]
        )
      ),
    ),
    );
  }

  Widget categoriaCabecera()
  {
    return Column(
      children: [
        Container(
          height: 120,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0)
          ),
        )
      ],
    );
  }
}
