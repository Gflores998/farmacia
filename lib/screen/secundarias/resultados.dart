import 'package:farmacia/models/producto_model.dart';
import 'package:farmacia/screen/secundarias/producto_detalle.dart';
import 'package:farmacia/services/crud_service.dart';
import 'package:farmacia/widgets/app_bar_comun.dart';
import 'package:farmacia/widgets/fila_producto.dart';
import 'package:flutter/material.dart';

class ResultadosPage extends StatefulWidget {
  final String filtro;
  const ResultadosPage({ Key? key, required this.filtro }) : super(key: key);

  @override
  State<ResultadosPage> createState() => _ResultadosPageState();
}

class _ResultadosPageState extends State<ResultadosPage> {

  late String valorFiltro;

  @override
  void initState() {
    valorFiltro = widget.filtro;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppBarComun(),
      body: SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(
              height: 20.0,
            ),
            RichText(
              text: TextSpan(
                children: [
                  const TextSpan(
                    text: "Mostrando Resultados para",
                    style: TextStyle(
                      fontSize: 14,
                      color: Color.fromRGBO(152, 156, 173, 1)
                    )
                  ),
                  TextSpan(
                    text: ' ' +valorFiltro,
                    style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: Color.fromRGBO(152, 156, 173, 1)
                    )
                  )
                ]
              ),
            ),
            const SizedBox(
              height: 30.0,
            ),
            StreamBuilder<List<Producto>>(
              stream: CrudService().obtenerProductosFiltrados(valorFiltro),
              builder: (context, snapshot) {
                if(!snapshot.hasData){
                  return const Center(
                    child: Text("No Data")
                  );
                } else if(snapshot.hasError){
                  return const Center(
                    child: Text("An Error Occured")
                  );
                } else if(snapshot.hasData) {
                  return ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      Producto producto = snapshot.data![index];
                      return FilaProducto(producto: producto);
                    }, 
                    separatorBuilder: (BuildContext context, int index) {
                      return const SizedBox( height: 10.0);
                    }, 
                    itemCount: snapshot.data!.length
                  );
                } else {
                  return const Center(child: Text('Algo Salió mál'));
                }
              },
            )
          ]
        )
      ),
    ),
    );
  }
}