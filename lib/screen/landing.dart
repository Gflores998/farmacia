import 'package:farmacia/providers/theme_provider.dart';
import 'package:farmacia/screen/auth/login.dart';
import 'package:farmacia/screen/auth/register.dart';
import 'package:farmacia/utils/style.dart';
import 'package:farmacia/widgets/btn_comun.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class LandingPage extends StatefulWidget {
  const LandingPage({ Key? key }) : super(key: key);

  @override
  State<LandingPage> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {

  @override
  Widget build(BuildContext context) {

    final themeProvider = Provider.of<ThemeProvider>(context);

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 30),
                child: Image.asset("assets/images/home.jpg"),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32.0),
                child: Text(
                  "Bienvenido a Punto Farmacia",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).primaryColor
                  ),
                )
              ),
              const SizedBox( height: 15),
              Text("Realiza tus compras desde la cualquier lugar, a cualquier hora",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 15,
                  color: themeProvider.isDarkMode ? MyStyle.textBodyDark : MyStyle.textBodyLigth
                ),
              ),
              const SizedBox( height: 25),
              BtnComun(
                text: 'Iniciar Sesión', 
                onPressed: () => {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) =>
                        const LoginPage(),
                    ),
                  )
                }, 
                esPrimario: false
              ),
              const SizedBox( height: 10),
              BtnComun(
                text: 'Registrarse', 
                onPressed: () => {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) =>
                        const RegisterPage(),
                    ),
                  )
                }, 
                esPrimario: true
              )
            ],
          ),
        ),
      ),
    );
  }
}