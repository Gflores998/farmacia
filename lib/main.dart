import 'package:farmacia/providers/theme_provider.dart';
import 'package:farmacia/screen/home.dart';
import 'package:farmacia/screen/landing.dart';
import 'package:farmacia/utils/style.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  late User? user;

  Future<FirebaseApp> _initializeFirebase() async {
    FirebaseApp firebaseApp = await Firebase.initializeApp();
    user = FirebaseAuth.instance.currentUser;
    return firebaseApp;
  }

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
    create: (context) => ThemeProvider(),
    builder: (context, _) {
      final themeProvider = Provider.of<ThemeProvider>(context);
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        themeMode: themeProvider.themeMode,
        theme: MyStyle.lightTheme,
        darkTheme: MyStyle.darkTheme,
        home: FutureBuilder(
          future: _initializeFirebase(),
          builder: (_, snapshot) {
            if(snapshot.hasError) {
              return const Text('Al parecer algo salio mál!');
            } else if (snapshot.hasData){
              if (user != null) {
                return const HomePage();
              } else {
                return const LandingPage();
              }
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        )
      );
    },
  );
}