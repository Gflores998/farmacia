import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:farmacia/models/carrito_model.dart';
import 'package:farmacia/models/categoria_model.dart';
import 'package:farmacia/models/favorito_model.dart';
import 'package:farmacia/models/producto_model.dart';
import 'package:farmacia/models/sucursal_model.dart';

class CrudService {
  
  final _db = FirebaseFirestore.instance;

  Stream<List<Categoria>> obtenerCategorias() {
    try {
      return _db.collection("categorias").snapshots().map((categorias) {
        final List<Categoria> categoriasFire = <Categoria>[];
        for (final DocumentSnapshot<Map<String, dynamic>> doc in categorias.docs) {
          categoriasFire.add(Categoria.fromDocumentSnapshot(doc: doc));
        }
        return categoriasFire;
      });
    } catch(e) {
        rethrow;
    }
  }

  Stream<List<Producto>> obtenerProductosPorCategoria(categoriaId) {
    try {
      return _db.collection("productos").where('categoriaId', isEqualTo: categoriaId).snapshots().map((productos) {
        final List<Producto> productosFire = <Producto>[];
        for(final DocumentSnapshot<Map<String, dynamic>> doc in productos.docs) {
          productosFire.add(Producto.fromDocumentSnapshot(doc: doc));
        }
        return productosFire;
      });
    } catch(e) {
      rethrow;
    }
  }

  Stream<List<Favorito>> obtenerFavoritos(uid) {
    try {
      return _db.collection("favoritos").snapshots().map((productos) {
        final List<Favorito> favoritosFire = <Favorito>[];
        for(final DocumentSnapshot<Map<String, dynamic>> doc in productos.docs) {
          favoritosFire.add(Favorito.fromDocumentSnapshot(doc: doc));
        }
        return favoritosFire;
      });
    } catch(e) {
      rethrow;
    }
  }

  Stream<List<Carrito>> obtenerCarrito(uid) {
    try {
      return _db.collection("carrito").snapshots().map((carrito) {
        final List<Carrito> carritoFire = <Carrito>[];
        for(final DocumentSnapshot<Map<String, dynamic>> doc in carrito.docs) {
          carritoFire.add(Carrito.fromDocumentSnapshot(doc: doc));
        }
        return carritoFire;
      });
    } catch(e) {
      rethrow;
    }
  }

  Stream<List<Producto>> obtenerProductosFiltrados(filtro) {
    try {
      return _db.collection("productos").where('nombre', isLessThanOrEqualTo: filtro)
      .limit(1).snapshots().map((productos) {
        final List<Producto> productosFire = <Producto>[];
        for(final DocumentSnapshot<Map<String, dynamic>> doc in productos.docs) {
          productosFire.add(Producto.fromDocumentSnapshot(doc: doc));
        }
        return productosFire;
      });
    } catch(e) {
        rethrow;
    }
  }

  Future<Producto> productoDetalle(id) async {
    var doc = _db.collection('productos').doc(id).get().then((value) {
      DocumentSnapshot<Map<String, dynamic>> doc = value;
      return Producto.fromDocumentSnapshot(doc: doc);
    });
    return doc;
  }

  Future<void> agregarFavorito(Favorito fav) async {
    _db.collection('favoritos').add(fav.toMap());
  }

  Future<void> agregarAlCarrito(Carrito car) async {
    _db.collection('carrito').add(car.toMap());
  }

  Future<Favorito> existeFav(productoId, userId) async {
    try {
       var existe = await _db.collection('favoritos').where('productoId', isEqualTo: productoId)
       .where('usuarioId', isEqualTo: userId).limit(1).get().then((value){
          if (value.docs != null && value.docs.isNotEmpty) {
            DocumentSnapshot<Map<String, dynamic>> doc = value.docs.first;
            return Favorito.fromDocumentSnapshot(doc: doc);
          } else {
            return Favorito(id: '', productoId: '', usuarioId: '', nombre: '');
          }
       });
      return existe;
    } catch (e) {
      rethrow;
    }
  }

  Future<void> eliminarFavorito(id) async {
    _db.collection('favoritos').doc(id).delete();
  }

  Future<void> eliminarDelCarrito(id) async {
    _db.collection('carrito').doc(id).delete();
  }

  Future<List<Sucursal>> obtenerSucursales() async {
    final List<Sucursal> sucFire = <Sucursal>[];
    await _db.collection('sucursales').get().then((event) {
      for(final DocumentSnapshot<Map<String, dynamic>> doc in event.docs) {
        sucFire.add(Sucursal.fromDocumentSnapshot(doc: doc));
      }
    });

    return sucFire;
  }

}