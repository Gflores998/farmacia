import 'package:cloud_firestore/cloud_firestore.dart';

class Sucursal {
  String id;
  String nombre;
  String descripcion;
  double lat;
  double lng;
  String direccion;
  String horario;

  Sucursal({
    required this.id,
    required this.nombre,
    required this.descripcion,
    required this.lat,
    required this.lng,
    required this.horario,
    required this.direccion,
  });

  factory Sucursal.fromDocumentSnapshot({
    required DocumentSnapshot<Map<String,dynamic>> doc
  }) {
    return Sucursal(
      id: doc.id,
      nombre: doc.data()!["nombre"],
      descripcion: doc.data()!["descripcion"],
      lat: doc.data()!["lat"],
      lng: doc.data()!["lng"],
      horario: doc.data()!["horario"],
      direccion: doc.data()!["direccion"],
    );
  }
}


