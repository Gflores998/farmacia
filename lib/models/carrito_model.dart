import 'package:cloud_firestore/cloud_firestore.dart';

class Carrito {
  String id;
  String usuarioId;
  String productoId;
  String nombre;
  int cantidad;
  double precio;

  Carrito({
    required this.id,
    required this.usuarioId,
    required this.productoId,
    required this.nombre,
    required this.cantidad,
    required this.precio
  });

  factory Carrito.fromDocumentSnapshot({
    required DocumentSnapshot<Map<String, dynamic>> doc
  }) {
    return Carrito(
      id: doc.id,
      usuarioId: doc.data()!["usuarioId"],
      productoId: doc.data()!["productoId"],
      nombre: doc.data()!["nombre"],
      cantidad: doc.data()!["cantidad"],
      precio: doc.data()!["precio"]
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "usuarioId": usuarioId,
      "productoId": productoId,
      "nombre": nombre,
      "cantidad": cantidad,
      "precio": precio
    };
  }

}