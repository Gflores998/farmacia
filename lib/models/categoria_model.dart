import 'package:cloud_firestore/cloud_firestore.dart';

class Categoria {
  String id;
  String nombre;
  String descripcion;
  String foto;

  Categoria({
    required this.id,
    required this.nombre,
    required this.descripcion,
    required this.foto,
  });

  factory Categoria.fromDocumentSnapshot({
    required DocumentSnapshot<Map<String,dynamic>> doc
  }) {
    return Categoria(
      id: doc.id,
      nombre: doc.data()!["nombre"],
      descripcion: doc.data()!["descripcion"],
      foto: doc.data()!["foto"]
    );
  }

}