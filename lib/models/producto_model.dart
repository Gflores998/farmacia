import 'package:cloud_firestore/cloud_firestore.dart';

class Producto {
  String id;
  String nombre;
  String descripcion;
  String breve;
  String foto;
  int disponible;
  bool oferta;
  double precio;
  double precioOferta;
  
  Producto({
    required this.id,
    required this.nombre,
    required this.descripcion,
    required this.breve,
    required this.foto,
    required this.disponible,
    required this.oferta,
    required this.precio,
    required this.precioOferta
  });

  factory Producto.fromDocumentSnapshot({
    required DocumentSnapshot<Map<String,dynamic>> doc
  }) {
    return Producto(
      id: doc.id,
      nombre: doc.data()!["nombre"],
      descripcion: doc.data()!["descripcion"],
      breve: doc.data()!["breve"],
      foto: doc.data()!["foto"],
      disponible: doc.data()!["disponible"],
      oferta: doc.data()!["oferta"],
      precio: doc.data()!["precio"],
      precioOferta: doc.data()!["precioOferta"]
    );
  }

}