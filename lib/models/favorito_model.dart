import 'package:cloud_firestore/cloud_firestore.dart';

class Favorito {
  String id;
  String usuarioId;
  String productoId;
  String nombre;

  Favorito({
    required this.id,
    required this.usuarioId,
    required this.productoId,
    required this.nombre
  });

  factory Favorito.fromDocumentSnapshot({
    required DocumentSnapshot<Map<String, dynamic>> doc
  }) {
    return Favorito(
      id: doc.id,
      usuarioId: doc.data()!["usuarioId"],
      productoId: doc.data()!["productoId"],
      nombre: doc.data()!["nombre"]
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "usuarioId": usuarioId,
      "productoId": productoId,
      "nombre": nombre
    };
  }

}