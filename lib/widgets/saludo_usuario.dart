import 'package:farmacia/utils/style.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:farmacia/providers/theme_provider.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SaludoUsuario extends StatefulWidget {
  const SaludoUsuario({Key? key}) : super(key: key);

  @override
  State<SaludoUsuario> createState() => _SaludoUsuarioState();
}

class _SaludoUsuarioState extends State<SaludoUsuario> {

  final FirebaseAuth auth = FirebaseAuth.instance;
  late User _currentUser;

  @override
  void initState() {
    _currentUser = auth.currentUser!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final themeProvider = Provider.of<ThemeProvider>(context);

    return Row(
      children: [
        const CircleAvatar(
          backgroundImage: NetworkImage(
            'https://firebasestorage.googleapis.com/v0/b/farmacia-837b4.appspot.com/o/usuarios%2Fprofile.png?alt=media&token=8708838d-70e3-4a64-a0ee-17779f266c6c'
          ),
          radius: 25,
          backgroundColor: Colors.transparent,
        ),
        const SizedBox(
          width: 15,
        ),
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: "Hola!, \n",
                style: TextStyle(
                  fontSize: 14,
                  color: themeProvider.isDarkMode ? MyStyle.textBodyDark : MyStyle.textBodyLigth
                )
              ),
              TextSpan(
                text: '${ _currentUser.displayName }',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: themeProvider.isDarkMode ? MyStyle.textBodyDark : MyStyle.textBodyLigth
                )
              )
            ]
          ),
        ),
        const Spacer(),
        IconButton(
          icon: const Icon(
            Icons.notifications
          ),
          onPressed: () {},
        )
      ],
    );
  }
}