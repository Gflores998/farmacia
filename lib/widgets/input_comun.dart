import 'package:flutter/material.dart';

class InputComun extends StatefulWidget {
  final String hintText;
  final bool obscureText;
  final Icon prefixIcon;
  final double borderRadius;
  final TextInputType keyboardType;
  final bool needValidation;
  final bool needValueInput;
  final TextEditingController textController;


  const InputComun({
    required this.textController,
    required this.hintText,
    this.obscureText = false,
    required this.prefixIcon,
    this.keyboardType = TextInputType.text,
    this.needValidation = false,
    this.needValueInput = false,
    this.borderRadius = 8.0,
    Key? key,
    ValueChanged<String>? onSubmit
  }) : super(key: key);

  @override
  State<InputComun> createState() => _InputComunState();
}

class _InputComunState extends State<InputComun> {
  late String _value;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 18.0),
      width: double.infinity,
      height: 50,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(widget.borderRadius),
        boxShadow: const [
          BoxShadow(
            color: Color.fromRGBO(169, 176, 185, 0.42),
            spreadRadius: 0,
            blurRadius: 8,
            offset: Offset(0, 2), // changes position of shadow
          ),
        ],
      ),
      child: TextFormField(
        obscureText: widget.obscureText,
        keyboardType: widget.keyboardType,
        controller: widget.textController,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Por favor agrege la información';
          }
          return null;
        },
        decoration: InputDecoration(
          prefixIcon: widget.prefixIcon,
          prefixIconConstraints: const BoxConstraints(minWidth: 0.0),
          border: InputBorder.none,
          hintText: widget.hintText,
          hintStyle: const TextStyle(
            fontSize: 14.0,
            color: Color.fromRGBO(169, 176, 185, 1),
          ),
        ),
      ),
    );
  }
}