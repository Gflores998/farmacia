import 'package:farmacia/models/producto_model.dart';
import 'package:farmacia/screen/secundarias/producto_detalle.dart';
import 'package:flutter/material.dart';

class FilaProducto extends StatefulWidget {
  final Producto producto;

  const FilaProducto({ Key? key, required this.producto }) : super(key: key);

  @override
  State<FilaProducto> createState() => _FilaProductoState();
}

class _FilaProductoState extends State<FilaProducto> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(
          builder: (_) =>
          ProductoDetalle(productoId: widget.producto.id)
        ));
      },
      child: Container(
        padding: const EdgeInsets.all(12.0),
        decoration: BoxDecoration(
          border: Border.all(
            color: const Color(0xFFF7F6FE)
          ),
          borderRadius: BorderRadius.circular(18.0)
        ),
        child: Row(
          children: [
            Container(
              height: 125.0,
              width: 125.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                    widget.producto.foto
                  ),
                  fit: BoxFit.fill
                )
              ),
            ),
            const SizedBox( width: 30.0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    widget.producto.nombre,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16
                    ),
                  ),
                  const SizedBox(height: 5.0),
                  Text(
                    widget.producto.descripcion,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      color: Color(0xFF70B6EB)
                    ),
                  ),
                  const SizedBox( height: 5.0),
                  Row(
                    children: [
                      widget.producto.oferta ?
                      Text(
                        "\$" + widget.producto.precio.toString(),
                        style: const TextStyle(
                          fontSize: 14.0,
                          decoration: TextDecoration.lineThrough
                        ),
                      )
                      : Text(
                        "\$" + widget.producto.precio.toString(),
                        style: const TextStyle(
                          fontSize: 14.0
                        ),
                      ),
                      widget.producto.oferta ? const Text(' | ', style: TextStyle(fontSize: 14)) : const Text(''),
                      widget.producto.oferta ? Text('\$' + widget.producto.precioOferta.toString(), style: const TextStyle(
                        fontSize: 14.0,
                        color: Color.fromRGBO(251, 89, 84, 1)
                      ),) : const Text('')
                    ],
                  )
                ],
              )
            )
          ],
        ),
      ),
    );
  }
}