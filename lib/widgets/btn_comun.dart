import 'package:flutter/material.dart';

class BtnComun extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final bool esPrimario;

  const BtnComun({Key? key, 
    required this.text, 
    required this.onPressed, 
    required this.esPrimario}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        width:  double.infinity,
        height: 50,
        decoration: BoxDecoration(
          color: esPrimario ? Theme.of(context).primaryColor : ThemeData().colorScheme.secondary,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: const [
            BoxShadow(
              color: Color.fromRGBO(169, 176, 185, 0.42),
              spreadRadius: 0,
              blurRadius: 8,
              offset: Offset(0, 2), // changes position of shadow
            ),
          ]
        ),
        child: Center(
          child: Text(
            text,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 18.0,
              fontWeight: FontWeight.w600,
            ),
          ),
        )
      )
    );
  }
}