import 'package:farmacia/screen/secundarias/carrito.dart';
import 'package:flutter/material.dart';

class AppBarComun extends StatefulWidget implements PreferredSizeWidget {

  @override
  Size get preferredSize => const Size.fromHeight(50);
  const AppBarComun({ Key? key }) : super(key: key);

  @override
  State<AppBarComun> createState() => _AppBarComunState();
}

class _AppBarComunState extends State<AppBarComun> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: IconButton(
        icon: const Icon(Icons.arrow_back, color: Colors.white),
        onPressed: () => Navigator.of(context).pop(),
      ),
      actions: [
        IconButton(
          icon: const Icon(Icons.shopping_cart),
          tooltip: 'Carrito',
          onPressed: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (context) =>
                    const CarritoPage(),
                ),
              );
          },
        )
      ]
    );
  }
}