import 'package:flutter/material.dart';

class BarraBusqueda extends StatefulWidget {

  final String hintText;
  final bool obscureText;
  final Icon suffixIcon;
  final Icon prefixIcon;
  final double borderRadius;

  final ValueChanged<String> onSubmit;

  const BarraBusqueda({Key? key, 
  required this.hintText, 
  required this.obscureText, 
  required this.suffixIcon, required this.prefixIcon, 
  required this.borderRadius, 
  required this.onSubmit}) : super(key: key);

  @override
  State<BarraBusqueda> createState() => _BarraBusquedaState();
}

class _BarraBusquedaState extends State<BarraBusqueda> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 18),
      width: double.infinity,
      height: 50,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(widget.borderRadius),
        boxShadow: const [
          BoxShadow(
            color: Color.fromRGBO(169, 176, 185, 0.42),
            spreadRadius: 0,
            blurRadius: 8,
            offset: Offset(0, 2)
          )
        ]
      ),
      child: TextFormField(
        obscureText: widget.obscureText,
        decoration: InputDecoration(
          prefixIcon: widget.prefixIcon,
          prefixIconConstraints: const BoxConstraints(minWidth: 0.0),
          border: InputBorder.none,
          hintText: widget.hintText,
          hintStyle: const TextStyle(
            fontSize: 14.0,
            color: Color.fromRGBO(169, 176, 185, 1)
          )
        ),
        onFieldSubmitted: (filter) {
          widget.onSubmit(filter);
        },
      ),
    );
  }
}